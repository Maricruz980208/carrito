from django.db import models
from django.utils import timezone


class Movimiento(models.Model):
    tipo_movimiento = models.ForeignKey('existencia.tipo_movimiento', on_delete=models.PROTECT)
    articulo = models.ForeignKey('productos.articulo', on_delete=models.PROTECT)
    fecha = models.DateField(default=timezone.now)
    cantidad = models.PositiveIntegerField()

    def __str__(self):
        return "%s (%s)" % (str(self.articulo), str(self.tipo_movimiento.codigo))

class Tipo_movimiento(models.Model):
    codigo = models.CharField('Código', max_length=1, unique=True)
    nombre = models.CharField(max_length=25)

    class Meta:
        verbose_name = 'Tipo de movimiento'
        verbose_name_plural = 'Tipos de movimiento'

    def __str__(self):
        return self.nombre
