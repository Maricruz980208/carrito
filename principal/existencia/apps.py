from django.apps import AppConfig


class ExistenciaConfig(AppConfig):
    name = 'existencia'
