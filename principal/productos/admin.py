from django.contrib import admin
from .models import *

class ArticuloAdmin(admin.ModelAdmin):
    model = Articulo
    list_display = ('codigo',
                    'nombre',
                    'descripcion',
                    'medida',
                    'envase',
                    'ultimo_precio_lista',
    )

    # def art_concepto(self, obj):
    #     return obj.__str__()
    # #     return obj.movimiento.concepto.descripcion
    # mov_concepto.short_description = 'Concepto'
    # mov_concepto.admin_order_field = 'movimiento__concepto__descripcion'

admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Medida)
admin.site.register(Envase)